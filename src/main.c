/*
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdbool.h>
#include <bananui/bananui.h>
#include <libalarmwakeup.h>
#include <libnotify/notify.h>

#define APP_ID "de.abscue.obp.Bananui.Clock"

enum typing_mode {
	TYPING_NONE,
	TYPING_HOURS,
	TYPING_MINUTES,
};

struct clock_app {
	GMainLoop *loop;
	bWindow *wnd;
	bMenu *menu;
	bMenuView *main_view;
	bBox *alarmlist;
	bButtonWidget *m_btn, *h_btn, *repeat_btn;
	struct alarmwakeup_file *aw;
	unsigned int minutes, hours;
	enum typing_mode typing;
	bool repeat;
};

struct alarm_data {
	struct clock_app *app;
	struct alarmwakeup_buf data;
};

static int handle_hours_button(bMenu *m, bButtonWidget *btn, void *arg);
static int handle_minutes_button(bMenu *m, bButtonWidget *btn, void *arg);
static int handle_repeat_button(bMenu *m, bButtonWidget *btn, void *arg);
static int handle_repeat_toggle(bMenu *m, void *arg);
static int handle_main_keydown(void *param, void *data);
static int handle_set_alarm_keydown(void *param, void *data);
static void list_alarms(struct clock_app *app);

B_MENU_ITEMS(set_alarm) = {
	{
		.name = "Hours",
		.func = B_BUTTON_FUNC_PUSHBUTTON,
		.style = "timeselect",
		.button_handler = handle_hours_button,
	},
	{
		.name = "Minutes",
		.func = B_BUTTON_FUNC_PUSHBUTTON,
		.style = "timeselect",
		.button_handler = handle_minutes_button,
	},
	{
		.name = "Repeat alarm",
		.func = B_BUTTON_FUNC_CHECKBOX,
		.style = "checkbox",
		.click_handler = handle_repeat_toggle,
		.button_handler = handle_repeat_button,
	},
};
B_DEFINE_MENU(set_alarm, "Set alarm") {
	bSetSoftkeyText(m->current->sk, "", "", "Save");
	bRegisterEventHandler(&m->current->card->keydown,
			      handle_set_alarm_keydown,
			      m->userdata);
}

B_MENU_ITEMS(alarm) = {};
B_DEFINE_MENU(alarm, "Alarm clock") {
	struct clock_app *app = m->userdata;

	bBoxRef(&app->alarmlist, m->current->card->box);

	bSetSoftkeyText(m->current->sk, "New", "DELETE", "");
	bRegisterEventHandler(&m->current->card->keydown,
			      handle_main_keydown,
			      m->userdata);

	list_alarms(app);
}

static int delete_alarm(void *param, void *data)
{
	struct alarm_data *alarm = data;
	struct clock_app *app = alarm->app;

	alarmwakeup_modify(app->aw, &alarm->data, false);

	g_idle_add_once((GSourceOnceFunc)list_alarms, app);

	return 1;
}

static int destroy_alarm_btn(void *param, void *data)
{
	g_free(data);

	return 1;
}

static void list_alarms(struct clock_app *app)
{
	struct alarm_data *alarm;

	if (!app->alarmlist)
		return;

	bDestroyBoxChildren(app->alarmlist);

	alarmwakeup_reopen(app->aw);

	alarm = g_new0(struct alarm_data, 1);

	while (alarmwakeup_read(app->aw, &alarm->data)) {
		char time[9];
		bButtonWidget *btn;

		if (0 != strcmp(alarm->data.label, "bananui"))
			continue;

		alarm->app = app;

		snprintf(time, sizeof(time), "%02d:%02d",
			 alarm->data.hour, alarm->data.minute);

		btn = bCreateButtonWidget(time, "menubutton", B_BUTTON_FUNC_PUSHBUTTON);
		bSetButtonSubtitle(btn, "alarm set");
		bRegisterEventHandler(&btn->box->click, delete_alarm, alarm);
		bRegisterEventHandler(&btn->box->destroy, destroy_alarm_btn, alarm);
		bAddBox(app->alarmlist, btn->box);

		alarm = g_new0(struct alarm_data, 1);
	}

	bRequestFrame(app->wnd);

	g_free(alarm);
}

static int handle_repeat_toggle(bMenu *m, void *arg)
{
	struct clock_app *app = m->userdata;

	app->repeat = !app->repeat;
	bSetButtonState(app->repeat_btn, app->repeat);
	bRequestFrame(app->wnd);
	return 1;
}

static int handle_repeat_button(bMenu *m, bButtonWidget *btn, void *arg)
{
	struct clock_app *app = m->userdata;

	app->repeat_btn = btn;
	if (btn)
		bSetButtonState(btn, app->repeat);
	return 1;
}

static void fmt_hours(struct clock_app *app)
{
	char hours[3];

	/* TODO: use 12-hour format based on user preference */
	hours[0] = '0' + (app->hours / 10);
	hours[1] = '0' + (app->hours % 10);
	hours[2] = '\0';
	bSetText(app->h_btn->subtitle, hours);
	bInvalidateWithChildren(app->h_btn->box);
	bRequestFrame(app->wnd);
}

static void fmt_minutes(struct clock_app *app)
{
	char minutes[3];

	minutes[0] = '0' + (app->minutes / 10);
	minutes[1] = '0' + (app->minutes % 10);
	minutes[2] = '\0';
	bSetText(app->m_btn->subtitle, minutes);
	bInvalidateWithChildren(app->h_btn->box);
	bRequestFrame(app->wnd);
}

static int handle_hours_button(bMenu *m, bButtonWidget *btn, void *arg)
{
	struct clock_app *app = m->userdata;

	app->h_btn = btn;
	if (btn)
		fmt_hours(app);
	return 1;
}

static int handle_minutes_button(bMenu *m, bButtonWidget *btn, void *arg)
{
	struct clock_app *app = m->userdata;

	app->m_btn = btn;
	if (btn)
		fmt_minutes(app);
	return 1;
}

static void update_hours(struct clock_app *app, unsigned int change)
{
	app->hours = (app->hours + 24 + change) % 24;
	if (app->h_btn)
		fmt_hours(app);
}

static void update_minutes(struct clock_app *app, unsigned int change)
{
	app->minutes = (app->minutes + 60 + change) % 60;
	if (app->m_btn)
		fmt_minutes(app);
}

static void type_hours(struct clock_app *app, unsigned int num)
{
	unsigned int hours = num;
	
	if (app->typing == TYPING_HOURS)
		hours += app->hours * 10;

	if (num > 2 || app->typing == TYPING_HOURS) {
		app->typing = TYPING_NONE;
		bFocusBox(app->wnd, app->menu->current->card, app->m_btn->box);
	} else {
		app->typing = TYPING_HOURS;
	}

	if (hours >= 24)
		app->hours = num;
	else
		app->hours = hours;

	fmt_hours(app);
}

static void type_minutes(struct clock_app *app, unsigned int num)
{
	unsigned int minutes = num;
	
	if (app->typing == TYPING_MINUTES)
		minutes += app->minutes * 10;

	if (num > 5 || app->typing == TYPING_MINUTES)
		app->typing = TYPING_NONE;
	else
		app->typing = TYPING_MINUTES;

	if (minutes >= 60)
		app->minutes = num;
	else
		app->minutes = minutes;

	fmt_minutes(app);
}

static bool get_number_key(xkb_keysym_t sym, unsigned int *num)
{
	switch (sym) {
	case XKB_KEY_0: *num = 0; break;
	case XKB_KEY_1: *num = 1; break;
	case XKB_KEY_2: *num = 2; break;
	case XKB_KEY_3: *num = 3; break;
	case XKB_KEY_4: *num = 4; break;
	case XKB_KEY_5: *num = 5; break;
	case XKB_KEY_6: *num = 6; break;
	case XKB_KEY_7: *num = 7; break;
	case XKB_KEY_8: *num = 8; break;
	case XKB_KEY_9: *num = 9; break;
	default:
		return false;
	}

	return true;
}

static void save_alarm(struct clock_app *app)
{
	struct alarmwakeup_buf alarm = {
		.hour = app->hours,
		.minute = app->minutes,
		.label = "bananui",
	};

	snprintf(alarm.command, sizeof(alarm.command),
		 "bananui-clock --trigger-alarm %02d:%02d:%c",
		 app->hours, app->minutes, app->repeat ? 'r' : 'o');

	alarmwakeup_modify(app->aw, &alarm, true);
	list_alarms(app);

	bPopMenu(app->menu, B_MENU_TRANSITION_FADE);
}

static int handle_set_alarm_keydown(void *param, void *data)
{
	struct clock_app *app = data;
	bKeyEvent *ev = param;
	unsigned int num;
	bBox *focus = app->menu->current->card->focus;

	if (ev->sym == XKB_KEY_Left) {
		if (app->m_btn && focus == app->m_btn->box)
			update_minutes(app, -1);
		else if (app->h_btn && focus == app->h_btn->box)
			update_hours(app, -1);
	} else if (ev->sym == XKB_KEY_Right) {
		if (app->m_btn && focus == app->m_btn->box)
			update_minutes(app, +1);
		else if (app->h_btn && focus == app->h_btn->box)
			update_hours(app, +1);
	} else if (ev->sym == BANANUI_KEY_SoftRight || ev->sym == XKB_KEY_F2) {
		save_alarm(app);
	} else if (get_number_key(ev->sym, &num)) {
		if (app->m_btn && focus == app->m_btn->box)
			type_minutes(app, num);
		else if (app->h_btn && focus == app->h_btn->box)
			type_hours(app, num);
	}

	return 1;
}

static int handle_main_keydown(void *param, void *data)
{
	struct clock_app *app = data;
	bKeyEvent *ev = param;

	if (ev->sym == BANANUI_KEY_SoftLeft || ev->sym == XKB_KEY_F1)
		B_OPEN_MENU(app->menu, set_alarm, B_MENU_TRANSITION_FADE);

	return 1;
}

static int handle_keydown(void *param, void *data)
{
	struct clock_app *app = data;
	bKeyEvent *ev = param;

	if (ev->sym == BANANUI_KEY_EndCall) {
		g_main_loop_quit(app->loop);
	} else if (ev->sym == BANANUI_KEY_Back) {
		if (bPopMenu(app->menu, B_MENU_TRANSITION_FADE) == 0) {
			g_main_loop_quit(app->loop);
		}
		return 0;
	}

	return 1;
}

static int render_app(void *param, void *data)
{
	struct clock_app *app = data;
	bRenderMenu(app->menu);
	return 1;
}

int main(int argc, char * const *argv)
{
	struct clock_app app = {};

	bDefaultTheme(bLoadScript("themes/clock.ipt"));

	app.aw = alarmwakeup_open();

	if (argc >= 3 && 0 == strcmp(argv[1], "--trigger-alarm")) {
		NotifyNotification *notification;
		GError *err = NULL;
		struct alarmwakeup_buf alarm = { .label = "bananui" };
		char time[9];
		char ch;

		if (3 == sscanf(argv[2], "%hhd:%hhd:%c",
				&alarm.hour, &alarm.minute, &ch) &&
		    ch == 'o') {
			/* alarm with 'o' should only trigger once */
			snprintf(alarm.command, sizeof(alarm.command),
				 "bananui-clock --trigger-alarm %s",
				 argv[2]);
			alarmwakeup_modify(app.aw, &alarm, false);
		}

		alarmwakeup_close(app.aw);

		snprintf(time, sizeof(time), "%02d:%02d", alarm.hour, alarm.minute);

		notify_init(APP_ID);

		notification = notify_notification_new("Alarm", time, "alarm-symbolic");
		notify_notification_set_urgency(notification, NOTIFY_URGENCY_CRITICAL);
		notify_notification_set_category(notification, "x-bananui.alarm");
		if (!notify_notification_show(notification, &err))
			g_error("Failed to show alarm: %s", err->message);

		return 0;
	}

	app.loop = g_main_loop_new(NULL, TRUE);
	app.wnd = bCreateWindow("Clock", APP_ID);
	if (!app.wnd)
		return 1;

	app.menu = bCreateMenuView(app.wnd);
	app.menu->userdata = &app;

	B_OPEN_MENU(app.menu, alarm, B_MENU_TRANSITION_NONE);

	app.main_view = app.menu->current;

	bRegisterEventHandler(&app.wnd->keydown, handle_keydown, &app);
	bRegisterEventHandler(&app.wnd->redraw, render_app, &app);
	bRequestFrame(app.wnd);

	g_main_loop_run(app.loop);

	alarmwakeup_close(app.aw);

	return 0;
}
